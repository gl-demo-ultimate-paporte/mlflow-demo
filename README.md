# GitLab Model Experiments + MlFlow example

This is a sample repository that showcases the MlFlow client integration to Model experiments. It is supported for Python 3.10.12. It is not supported for Python versions > 3.10.

* `train.py`: Script that trains a simple model and logs to MlFlow/GitLab
* `train.ipynb`: Same as train.py, but in notebook format

## Running `train.py`

Install either through pip or Poetry. Poetry use virtual environments to manage versions, that you can install using [https://python-poetry.org/docs/#installation](https://python-poetry.org/docs/#installation).

### MlFlow backend

1. Install dependencies
    ```bash
    poetry install
    ```
    or 
    ```bash
    pip install -r requirements.txt
    ```

2. Start MlFlow
   ```bash
   mlflow ui   
   ```
   
3. Run the script
   ```bash
   python train.py
   ```

### GitLab Model experiments

1. Install dependencies
   ```bash
   poetry install
   ```
   or 
   ```bash
   pip install -r requirements.txt
   ```

2. Set enviromental variables

   You need only one of the first or last 2 environment variables depending on how you run it (poetry or locally).
   ```bash
   export MLFLOW_TRACKING_URI="https://gitlab.com/api/v4/projects/$PROJECT_ID/ml/mlflow"
   export MLFLOW_TRACKING_TOKEN="<your api token>"
   export POETRY_MLFLOW_TRACKING_URI="https://gitlab.com/api/v4/projects/$PROJECT_ID/ml/mlflow"
   export POETRY_MLFLOW_TRACKING_TOKEN="<your api token>" 
   ```
   
3. Run the script
   ```bash
   poetry run python train.py
   ```

   ```bash
   python train.py
   ```
   
## Helper files


* `api_parity.py` compares the differences between MlFlow API as implemented on GitLab and regular
* `results.md` Results of running `api_partity.py`
